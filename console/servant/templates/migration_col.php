<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class ${migration_name}_${time} {
    public function up() {
        Capsule::schema()->table('${table_name}', function($table) {
            $table->text('ccomment')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->table('${table_name}', function($table) {

        });
    }
}