<?php

define('PRODUCTION_PATH', '/var/www/hozyain.su/data/www/hozyain.su');


class RoboFile extends \Robo\Tasks
{

    public function deploy()
    {
        $this->taskRsync()
            ->fromPath('../')
            ->toHost('5.45.127.124')
            ->toUser('root')
            ->toPath(PRODUCTION_PATH)
            ->recursive()
            ->excludeVcs()
            ->checksum()
            ->wholeFile()
            ->exclude('vendor')
            ->exclude('.idea')
            ->exclude('configs/.env')
            ->exclude('data')
            ->exclude('storage/tnt/products')
            ->exclude('/public/assets/public/node_modules')
            ->exclude('/public/imagecache')
            ->exclude('/public/sitemap.xml')
            ->exclude('composer.lock')
            ->verbose()
            ->progress()
            ->humanReadable()
            ->stats()
            ->run();
    }
}