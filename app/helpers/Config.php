<?php

namespace App\Helpers;

class Config
{
    public $config;

    private static $instance;

    private function __construct($container = null)
    {
        if ($container){
            $this->config['container'] = $container;
            $this->config['user'] = $container->sentinel->getUser();
        }
    }

    public static function getInstance($container = null)
    {
        if (empty(self::$instance)){
            self::$instance = new Config($container);
        }
        return self::$instance->config;
    }

}