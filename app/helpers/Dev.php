<?php

namespace App\Helpers;

use Curl\Curl;
use Illuminate\Database\Capsule\Manager as DB;

/**
 * Класс со вспомогательными функциями для разработчика
 */
class Dev
{
    // Путь к .env
    const ENV_PATH = __DIR__ . '/../../configs/.env';


    /**
     * Отправляет сообщение в телеграмм (не забываем про прокси, провайдеры блокируют запросы к телеге)
     * @param string $message - текст
     * @param int $chat_id - чат (необязательно)
     * @throws \ErrorException
     */
    public static function sayTelegramm($message = 'тест', $chat_id = null)
    {
        $chat_id = is_null($chat_id) ? getenv('TELEGRAM_ID') : $chat_id;

        if (!$chat_id) {
            if (!$chat_id) {
                throw new \Exception('TELEGRAM_ID not found in "' . self::ENV_PATH . '"');
            }
        }
        
        $curl = new Curl();
        $url = "https://api.telegram.org/bot222240867:AAFuxnbRw7_Zy-3xAlUgQxvYe-dvAwKQG8s/";
        $params = [
            'chat_id' => $chat_id,
            'text' => $message,
        ];
        $curl->post($url . 'sendMessage', $params);
    }

    /**
     *  Позволяет посмотреть время выполнения SQL для следующего запроса после этой функции
     */
    public static function debugDB()
    {
        DB::listen(function ($sql) {
            ddd($sql);
        });

//        DB::listen(function($sql, $bindings, $time) {
//            ddd($sql, $bindings, $time);
//        });
    }


    /**
     * Создает/изменяет переменную в файле .env
     * @param string $var
     * @param string $value
     * @return bool
     * @throws \Exception
     */
    public static function setDotEnv($var, $value)
    {
        $env = self::getEnvArr();
        $status = false;

        foreach ($env as &$row) {
            if (substr($row, 0, 1) === '#') {
                continue;
            }

            if (stripos($row, $var) !== false) {
                $row = self::dotEnvString($var, $value);
                $status = true;
            }
        }

        // Если переменная не найдена, создаем
        if (!$status) {
            array_push($env,self::dotEnvString($var, $value));
            $status = true;
        }

        file_put_contents(self::ENV_PATH, implode(PHP_EOL, $env));

        return $status;
    }

    /**
     * Получает значение переменной из файла .env
     * @param $var
     * @return mixed|string|null
     * @throws \Exception
     */
    public static function getDotEnv($var)
    {
        $env = self::getEnvArr();
        $result = null;

        foreach ($env as &$row) {
            if (substr($row, 0, 1) === '#') {
                continue;
            }

            if (stripos($row, $var) !== false) {
                $result = $row;
                break;
            }
        }

        if (!is_null($result)){
            $result = trim(explode('=', $result)[1]);
            $result = str_replace('"', '', $result);
        }

        return $result;
    }

    /**
     * Создает строку типа DB_DRIVER = "mysql"
     * @param $var
     * @param $value
     * @return string
     */
    public static function dotEnvString($var, $value)
    {
        return $var . ' = "' . $value . '"';
    }

    public static function getEnvArr()
    {
        if (!is_file(self::ENV_PATH)) {
            throw new \Exception('Your .env not found in ' . self::ENV_PATH);
        }

        $env = trim(file_get_contents(self::ENV_PATH));
        $env = explode(PHP_EOL, $env);
        return $env;
    }
}