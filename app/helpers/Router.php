<?php


namespace App\Helpers;


use App\Models\Floor;
use MongoDB\Client;

class Router
{
    public $floor;

    public static function dstr($floor_id, $from, $to)
    {
        $floor = Floor::with('tops')->find($floor_id);
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->tracker->{$floor->uuid};

        $routes = [];

        $fields = $floor->tops->pluck('id')->toArray();

        $cursor = $collection->find([], $fields);
        foreach ($cursor as $row) {
            $row = $row->getArrayCopy();
            $id = $row['id'];
            unset($row['_id']);
            unset($row['id']);
            foreach ($row as $vertex => $price) {
                $routes[] = ['from' => $id, 'to' => $vertex, 'price' => intval($price)];
            }
        };

        return self::getShortestPath($from, $to, $routes);
    }

    public static function getShortestPath($from_name, $to_name, $routes)
    {
        $graph = new Graph();
        foreach ($routes as $route) {
            $from = $route['from'];
            $to = $route['to'];
            $price = $route['price'];
            if (!array_key_exists($from, $graph->getNodes())) {
                $from_node = new Node($from);
                $graph->add($from_node);
            } else {
                $from_node = $graph->getNode($from);
            }
            if (!array_key_exists($to, $graph->getNodes())) {
                $to_node = new Node($to);
                $graph->add($to_node);
            } else {
                $to_node = $graph->getNode($to);
            }
            $from_node->connect($to_node, $price);
        }

        $g = new Dijkstra($graph);
        $start_node = $graph->getNode($from_name);
        $end_node = $graph->getNode($to_name);
        $g->setStartingNode($start_node);
        $g->setEndingNode($end_node);
        return [ 'routing' => $g->getPath(), 'distance' => $g->getDistance()];
    }
}