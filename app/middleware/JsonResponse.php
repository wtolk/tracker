<?php

namespace App\Middleware;

class JsonResponse
{
    private $container;

    public function __construct($container) {
        $this->container = $container;
    }

    public function __invoke($request, $response, $next)
    {
        $newResponse = $response->withHeader('Content-type', 'application/json');
        return $response = $next($request, $newResponse);
    }
}