<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_requests_movement_1569791333 {
	public function up() {
		Capsule::schema()->table('requests_movement', function($table) {
            $table->integer('worker_id')->nullable();
		});
	}

}

