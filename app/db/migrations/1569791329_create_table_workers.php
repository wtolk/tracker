<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_workers_1569791329 {
	public function up() {
		Capsule::schema()->create('workers', function($table) {
			$table->increments('id')->unsigned();
			$table->string('name');
            $table->char('phone', 12);
            $table->integer('status');
		});
	}

}

