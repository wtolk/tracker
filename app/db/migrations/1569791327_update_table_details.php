<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_details_1569791327 {
	public function up() {
		Capsule::schema()->table('details', function($table) {
            $table->dropColumn('article');
            $table->dropColumn('storage_id');
            $table->dropColumn('storage_cell');
		});
	}

}

