<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_buildings_1569729524 {
    public function up() {
        Capsule::schema()->create('buildings', function($table) {
            $table->increments('id');
			$table->string('title');
        });
    }
}
