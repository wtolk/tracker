<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_role_users_1530860522 {
    public function up() {
        Capsule::schema()->create('role_users', function($table) {
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('role_id');
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->table('$table_name', function($table) {

        });
    }
}
