<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_detail_entities_1569791326 {
	public function up() {
		Capsule::schema()->create('detail_entities', function($table) {
			$table->increments('id')->unsigned();
			$table->string('article');
            $table->integer('detail_id');
            $table->integer('storage_id');
            $table->integer('storage_cell');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
		});
	}

	public function down() {
		Capsule::schema()->dropIfExists('places');
	}
}

