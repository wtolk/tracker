<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_zones_1569729655 {
    public function up() {
        Capsule::schema()->create('zones', function($table) {
            $table->increments('id');
			$table->string('title')->nullable();
			$table->text('geo')->nullable();
			$table->integer('floor_id')->nullable();
        });
    }
}
