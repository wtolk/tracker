<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_paths_1569729640 {
    public function up()
    {
        Capsule::schema()->create('paths', function ($table) {
            $table->integer('f_top_id')->nullable();
            $table->integer('s_top_id')->nullable();
            $table->float('score')->nullable();
            $table->text('geo')->nullable();
        });
    }
}
