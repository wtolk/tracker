<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_places_1569789525 {
	public function up() {
		Capsule::schema()->create('places', function($table) {
			$table->increments('id')->unsigned();
			$table->string('title');
			$table->string('type')->default('sklad');
		});
	}

	public function down() {
		Capsule::schema()->dropIfExists('places');
	}
}

