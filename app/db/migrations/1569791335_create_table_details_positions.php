<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_details_positions_1569791335 {
	public function up() {
		Capsule::schema()->create('details_positions', function($table) {
			$table->increments('id')->unsigned();
            $table->integer('entity_id');
            $table->integer('position_id');
		});
	}

}

