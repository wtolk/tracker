<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_details_requests_1569642525 {
	public function up() {
		Capsule::schema()->create('details_requests', function($table) {
			$table->integer('detail_id');
			$table->integer('request_id');
			$table->integer('quantity');
		});
	}

	public function down() {
		Capsule::schema()->dropIfExists('details_requests');
	}
}

