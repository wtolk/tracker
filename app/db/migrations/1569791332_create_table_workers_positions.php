<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_workers_positions_1569791332 {
	public function up() {
		Capsule::schema()->create('workers_positions', function($table) {
			$table->increments('id')->unsigned();
            $table->integer('worker_id');
            $table->text('options');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
		});
	}

}

