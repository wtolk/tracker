<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_floors_1569729593 {
    public function up() {
        Capsule::schema()->create('floors', function($table) {
            $table->increments('id');
			$table->string('plan_path')->nullable();
			$table->float('height')->nullable();
			$table->float('width')->nullable();
			$table->integer('floor')->nullable();
			$table->integer('building_id')->nullable();
			$table->string('uuid')->nullable();
        });
    }
}
