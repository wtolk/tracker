<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_storages_1569789470 {
	public function up() {
		Capsule::schema()->create('storages', function($table) {
			$table->increments('id')->unsigned();
			$table->string('title');
		});
	}

	public function down() {
		Capsule::schema()->dropIfExists('storages');
	}
}

