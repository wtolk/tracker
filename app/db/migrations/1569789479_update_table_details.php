<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_details_1569789479 {
	public function up() {
        Capsule::schema()->table('details', function($table) {
            $table->integer('quantity')->after('weight')->default(1);
        });
	}

}

