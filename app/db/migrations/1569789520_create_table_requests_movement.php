<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_requests_movement_1569789520 {
	public function up() {
		Capsule::schema()->create('requests_movement', function($table) {
			$table->increments('id')->unsigned();
			$table->integer('from');
			$table->integer('to');
			$table->timestamp('delivery_time');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
		});
	}

	public function down() {
		Capsule::schema()->dropIfExists('requests_movement');
	}
}

