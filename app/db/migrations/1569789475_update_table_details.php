<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class update_table_details_1569789475 {
	public function up() {
        Capsule::schema()->table('details', function($table) {
            $table->dropColumn('volume');
            $table->integer('width')->after('weight');
            $table->integer('heigth')->after('weight');
            $table->integer('length')->after('weight');
        });
	}

}

