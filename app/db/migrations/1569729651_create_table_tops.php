<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_tops_1569729651 {
    public function up() {
        Capsule::schema()->create('tops', function($table) {
            $table->increments('id');
			$table->integer('zone_id')->nullable();
			$table->integer('floor_id')->nullable();
			$table->text('geo')->nullable();
        });
    }

}
