<?php

use Illuminate\Database\Capsule\Manager as Capsule;

class create_table_details_1569618701 {
    public function up() {
        Capsule::schema()->create('details', function(\Illuminate\Database\Schema\Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('article');
            $table->text('description')->nullable();
            $table->string('units');
            $table->integer('weight');
            $table->integer('volume');
            $table->integer('storage_id');
            $table->integer('storage_cell');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->nullable();
        });
    }

    public function down() {
        Capsule::schema()->table('details', function($table) {

        });
    }
}
