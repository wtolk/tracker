<?php

use App\Controllers\BuildingController;
use App\Controllers\FloorController;
use App\Controllers\MapController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\TestController;
use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Access;
use App\Middleware\Registred;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\ModuleController;
use App\Controllers\DetailController;
use App\Controllers\WorkerController;
use App\Controllers\DetailEntityController;
use App\Controllers\RequestMovementController;
//delimiter//

$c = $app->getContainer();


$app->get('/login', UserController::class.':showLoginPage')->setName('user.showLoginPage');

$app->get('/message/no-access', UserController::class.':noAccess')->setName('error.noAccess');

$app->get('/', function (Request $request, Response $response) use ($c) {
    return $this->view->render($response, 'admin/layout/layout.twig');
})->add(new Registred($container));

$app->get('/test', TestController::class.':test');

$app->group('/admin', function() use($app, $c) {

    $app->get('/users', UserController::class . ':showAdminUserList')->setName("user.showAdminUserList")->add(new Access($c, ['user_view']));
    $app->get('/users/add', UserController::class . ':showAdminUserAdd')->setName("user.showAdminUserAdd")->add(new Access($c, ['user_view', 'user_create']));
    $app->get('/users/{id}', UserController::class . ':showAdminUserEdit')->setName('user.showAdminUserEdit')->add(new Access($c, ['user_view', 'user_update']));

    $app->get('/roles', RoleController::class . ':showAdminRoleList')->setName('role.showAdminRoleList')->add(new Access($c, ['roles_view']));

    $app->get('/navigation', NavigationController::class . ':showAdminNavigationList')->setName('navigation.showAdminNavigationList');
    $app->get('/navigation/add', NavigationController::class . ':showAdminNavigationAdd')->setName('navigation.showAdminNavigationAdd');
    $app->get('/navigation/{id}', NavigationController::class . ':showAdminNavigationEdit')->setName('navigation.showAdminNavigationEdit');

    $app->get('/navigation/{id}/items', NavigationItemController::class.':showadminNavigationItemList')->setName('navigation.showAdminNavigationItemList');
    $app->get('/navigation/{id}/items/add', NavigationItemController::class.':showAdminNavigationItemAdd')->setName('navigation.showAdminNavigationItemAdd');
    $app->get('/navigation/{id}/items/{item_id}', NavigationItemController::class.':showAdminNavigationItemEdit')->setName('navigation.showAdminNavigationItemEdit');

    $app->get('/modules', ModuleController::class . ':showAdminModuleList')->setName('module.showAdminModuleList')->add(new Access($c, ['modules_view']));


    $app->get('/map', MapController::class.':showAdminMap')->setName('map.map');
    $app->get('/map/add-building', MapController::class.':showAddBuilding')->setName('map.showAddBuilding');
    $app->get('/map/{id}', MapController::class.':showAdminMapEdit')->setName('map.mapedit');


    $app->group('/building', function () use ($app, $c){
        $app->get('', BuildingController::class.':showAdminBuildingsList')->setName('building.showAdminBuildingsList');
        $app->get('/add', BuildingController::class.':showAdminBuildingAdd')->setName('building.showAdminBuildingAdd');
        $app->get('/{id}', BuildingController::class.':showAdminBuildingEdit')->setName('building.showAdminBuildingEdit');

        $app->get('/{id}/floors', FloorController::class.':showAdminFloorsList')->setName('floor.showAdminFloorsList');
        $app->get('/{id}/floors/add', FloorController::class.':showAdminFloorAdd')->setName('floor.showAdminFloorAdd');
        $app->get('/{building_id}/floors/{id}', FloorController::class.':showAdminFloorEdit')->setName('floor.showAdminFloorEdit');

    });

})->add(new Registred($c));

$app->group('/admin', function() use($app, $c) {

    $app->get('/details', DetailController::class . ':showAdminDetailList')->setName('detail.showAdminDetailList');
    $app->get('/details/add', DetailController::class . ':showAdminDetailAdd')->setName('detail.showAdminDetailAdd');
    $app->get('/details/{id}', DetailController::class . ':showAdminDetailEdit')->setName('detail.showAdminDetailEdit');

})->add(new Registred($c));

$app->group('/admin', function() use($app, $c) {

    $app->get('/rm', RequestMovementController::class . ':showAdminRequestList')->setName('request.showAdminRequestList');
    $app->get('/rm/add', RequestMovementController::class . ':showAdminRequestAdd')->setName('request.showAdminRequestAdd');
    $app->get('/rm/{id}', RequestMovementController::class . ':showAdminRequestEdit')->setName('request.showAdminRequestEdit');

})->add(new Registred($c));

$app->group('/admin/detail-entity', function() use($app, $c) {

    $app->get('/{detail_id}/add', DetailEntityController::class . ':showAdminDetailEntityAdd')->setName('detailEntity.showAdminDetailEntityAdd');
    $app->get('/{id}', DetailEntityController::class . ':showAdminDetailEntityEdit')->setName('detailEntity.showAdminDetailEntityEdit');

})->add(new Registred($c));


$app->group('/admin', function() use($app, $c) {

    $app->get('/workers', WorkerController::class . ':showAdminWorkerList')->setName('worker.showAdminWorkerList');
    $app->get('/workers/{id}', WorkerController::class . ':showAdminWorkerEdit')->setName('worker.showAdminWorkerEdit');

})->add(new Registred($c));