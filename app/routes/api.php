<?php

use App\Controllers\BuildingController;
use App\Controllers\FloorController;
use App\Controllers\MapController;
use App\Controllers\RouteController;
use App\Controllers\TestController;
use App\Controllers\TopController;
use App\Controllers\ZoneController;
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Registred;
use App\Middleware\Access;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\ModuleController;
use App\Controllers\DetailController;
use App\Controllers\RequestMovementController;
use App\Controllers\DetailEntityController;
use App\Controllers\WorkerController;

//delimiter//

$app->post('/api/login', UserController::class . ':doLogin')->setName('user.doLogin');
$app->get('/api/logout', UserController::class . ':doLogout')->setName('user.doLogout');


$app->group('/api', function () use ($app, $c) {

    $app->post('/users', UserController::class . ':createUser')->setName('user.createUser')->add(new Access($c, ['user_create']));
    $app->post('/users/{id}', UserController::class . ':updateUser')->setName('user.updateUser')->add(new Access($c, ['user_update']));
    $app->post('/users/{id}/delete', UserController::class . ':deleteUser')->setName('user.deleteUser')->add(new Access($c, ['user_delete']));

    $app->post('/users/{id}/remove-avatar', UserController::class . ':deleteUserAvatar')->setName('user.deleteAvatar')->add(new Access($c, ['user_update']));;

    $app->post('/roles', RoleController::class . ':createRole')->setName('role.createRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/get_role', RoleController::class . ':getRole')->setName('role.getRole')->add(new Access($c, ['roles_view']));
    $app->post('/roles/create', RoleController::class . ':createUserRoles')->setName('role.createRole')->add(new Access($c, ['roles_view']));

    $app->post('/navigation/items/save', NavigationItemController::class . ':saveNavigationItems')->setName('navigation.saveNavigationItems');
    $app->post('/navigation/get-item-permissions', NavigationItemController::class . ':getItemPermissions')->setName('navigation.getItemPermissions');

    $app->post('/navigation', NavigationController::class . ':createNavigation')->setName('navigation.createNavigation');
    $app->post('/navigation/{id}', NavigationController::class . ':updateNavigation')->setName('navigation.updateNavigation');
    $app->get('/navigation/{id}/delete', NavigationController::class . ':deleteNavigation')->setName('navigation.deleteNavigation');

    $app->post('/navigation/{id}/item', NavigationItemController::class . ':createNavigationItem')->setName('navigation.createNavigationItem');
    $app->post('/navigation/{id}/item/{item_id}', NavigationItemController::class . ':updateNavigationItem')->setName('navigation.updateNavigationItem');
    $app->get('/navigation/{id}/item/{item_id}/delete', NavigationItemController::class . ':deleteNavigationItem')->setName('navigation.deleteNavigationItem');

    $app->post('/modules/switch-module', ModuleController::class . ':switchModule')->setName('module.switchModule');
    $app->post('/modules/add-module', ModuleController::class . ':addModule')->setName('module.addModule');


//    $app->post('/map/create', MapController::class . ':createBuilding')->setName('map.createBuilding');
//    $app->post('/map/load-plan', MapController::class . ':loadPlan')->setName('map.loadPlan');
//    $app->post('/map/set-coords', MapController::class . ':setCoords')->setName('map.setCoords');
//    $app->post('/map/save-polygons', MapController::class . ':savePolygons')->setName('map.savePolygons');


    $app->group('/building', function () use ($app, $c) {
        $app->post('', BuildingController::class . ':createBuilding')->setName('building.createBuilding');
        $app->post('/{id}', BuildingController::class . ':updateBuilding')->setName('building.updateBuilding');

        $app->post('/{building_id}/floor', FloorController::class . ':createFloor')->setName('floor.createFloor');
        $app->post('/{building_id}/floor/{id}', FloorController::class . ':updateFloor')->setName('floor.updateFloor');

        $app->post('/floor/{id}/save-zone', ZoneController::class . ':saveZone')->setName('zone.saveZone');
        $app->post('/floor/{id}/save-top', TopController::class . ':saveTop')->setName('top.saveTop');
        $app->post('/floor/{id}/save-path', TopController::class . ':savePath')->setName('top.savePath');

        $app->get('/floor/{id}/parse', TopController::class . ':parseFloor');
    });

    $app->post('/details', DetailController::class . ':createDetail')->setName('detail.createDetail');
    $app->post('/details/{id}', DetailController::class . ':updateDetail')->setName('detail.updateDetail');
    $app->get('/details/{id}/delete', DetailController::class . ':deleteDetail')->setName('detail.deleteDetail');

    $app->post('/rm', RequestMovementController::class . ':createRequest')->setName('request.createRequest');
    $app->post('/rm/{id}', RequestMovementController::class . ':updateRequest')->setName('request.updateRequest');
    $app->get('/rm/{id}/delete', RequestMovementController::class . ':deleteRequest')->setName('request.deleteRequest');

    $app->post('/detail-entity', DetailEntityController::class . ':createDetailEntity')->setName('detailEntity.createDetailEntity');
    $app->post('/detail-entity/{id}', DetailEntityController::class . ':updateDetailEntity')->setName('detailEntity.updateDetailEntity');
    $app->get('/detail-entity/{id}/delete', DetailEntityController::class . ':deleteDetailEntity')->setName('detailEntity.deleteDetailEntity');

    $app->post('/routing/map', RouteController::class.':getRouteWithMap')->setName('route.getRouteWithMap');
})->add(new Registred($container));



$app->post('/api/workers/positions', WorkerController::class . ':setWorkerPosition')->setName('worker.setWorkerPosition');

$app->get('/rm/{id}', TestController::class . ':rm');
