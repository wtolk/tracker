<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use App\Controllers\UserController;
use App\Controllers\RoleController;
use App\Middleware\Registred;
use App\Middleware\JsonResponse;
use App\Middleware\Access;
use App\Controllers\NavigationController;
use App\Controllers\NavigationItemController;
use App\Controllers\ModuleController;
use App\Controllers\DetailController;
use App\Controllers\RequestMovementController;


$app->group('/api/mobile', function() use($app, $c) {

    $app->get('/rm', RequestMovementController::class . ':getMobileRequestMovementsList')->setName('requestMovements.getMobileRequestMovementsList');

})->add(new JsonResponse($c));

