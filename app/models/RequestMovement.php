<?php

namespace App\Models;

class RequestMovement extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'requests_movement';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

    const UPDATED_AT = null;

    public function getDeliveryTimeAttribute($value)
    {
        if ($value == '0000-00-00 00:00:00') {
            return '2019-12-12 00:00:00';
        }
    }


    public function setDeliveryTimeAttribute($value)
    {
        $this->attributes['delivery_time'] = $value.':00';
    }

    public function placeForm()
    {
        return $this->hasOne(Zone::class, 'id', 'from');
    }

    public function placeTo()
    {
        return $this->hasOne(Zone::class, 'id', 'to');
    }

    public function details()
    {
        return $this->belongsToMany(Detail::class, 'details_requests', 'request_id', 'detail_id')->withPivot('quantity');
    }

    public function checkQuantityDetails()
    {
        foreach($this->details->toArray() as $detail) {
            if ($detail['pivot_quantity'] > $detail['quantity']) {
                echo 'У детали '.$detail['title'].' нет необходимого количества экземпляров.<br>'.PHP_EOL;
                echo 'Запрашиваемое количество: '.$detail['pivot_quantity'].' Фактически на складе: '.$detail['quantity'];
            }
        }
    }

    public function setDetailsList($array_json)
    {
        $data = json_decode($array_json, true);
        $details = [];
        $items = [];
        foreach ($data as $value) {
            $details[$value['title']]['pivot_quantity'] = $details[$value['title']]['pivot_quantity'] + $value['pivot_quantity'];
            $details[$value['title']]['title'] = $value['title'];
            $details[$value['title']]['id'] = $value['id'];
        }
//        ddd($details);
        foreach ($details as $key => $item) {
            $details[$key]['id'] = Detail::where('title', $item['title'])->first()->id;
            $items[Detail::where('title', $item['title'])->first()->id] = ['quantity' => $item['pivot_quantity']];
        }
//        ddd($items);
        $this->details()->sync($items);
    }

}