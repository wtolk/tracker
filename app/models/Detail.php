<?php

namespace App\Models;

class Detail extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'details';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];
    protected $appends = ['quantity', 'pivot_quantity'];

    public function itemsList()
    {
        return $this->hasMany(DetailEntity::class, 'detail_id', 'id');
    }

    public function getQuantityAttribute()
    {
        return $this->itemsList()->count();
    }

    public function getPivotQuantityAttribute()
    {
        return $this->pivot['quantity'];
    }

}