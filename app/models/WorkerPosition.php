<?php

namespace App\Models;

class WorkerPosition extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'workers_positions';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
        'options' => 'array',
    ];



    public function setOptionsAttribute($value)
    {
        $this->attributes['options'] = json_encode($value);
    }
}