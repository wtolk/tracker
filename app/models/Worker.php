<?php

namespace App\Models;

class Worker extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'workers';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];


    public function sendMap()
    {
        // Здесь запрос пуш уведомления на смартфон пользователя
        return '200';
    }

}