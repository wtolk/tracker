<?php

namespace App\Models;

class DetailEntity extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'detail_entities';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];

    public function storage()
    {
        return $this->hasOne(Zone::class, 'id', 'storage_id');
    }
}