<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Top extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'tops';

    public function loadPath()
    {
        $paths = Path::where('f_top_id', $this->id)->orWhere('s_top_id', $this->id)->get();
        ddd($paths);
    }
}