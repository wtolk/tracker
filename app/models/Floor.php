<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Floor extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'floors';
    protected $primaryKey = 'id';
    protected $checkbox = [];


    public function zones()
    {
        return $this->hasMany(Zone::class, 'floor_id', 'id');
    }

    public function tops()
    {
        return $this->hasMany(Top::class, 'floor_id', 'id');
    }
}