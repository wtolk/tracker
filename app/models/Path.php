<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Path extends Model
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'paths';
}