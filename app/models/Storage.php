<?php

namespace App\Models;

class Storage extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'storages';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];
    protected $casts = [
		'created_at' => 'datetime',
		'updated_at' => 'datetime',
	];




}