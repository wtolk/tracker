<?php


namespace App\Models;


class Building extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'buildings';
    protected $primaryKey = 'id';
    protected $checkbox = [];

    public function floors()
    {
        return $this->hasMany(Floor::class, 'building_id', 'id');
    }
}