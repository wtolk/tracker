<?php

namespace App\Models;

class Place extends CustomModel
{
    public $timestamps = false;
    protected $guarded = [];
    protected $table = 'places';
    protected $primaryKey = 'id';
    protected $checkbox = [];
    protected $attributes = [];


}