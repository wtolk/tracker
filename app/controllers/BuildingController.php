<?php


namespace App\Controllers;

use App\Models\Building;

class BuildingController extends Controller
{
    public function showAdminBuildingsList()
    {
        $buildings = Building::get()->toArray();
        $this->twig_vars['buildings'] = $buildings;
        return $this->render('admin/buildings/list.twig');
    }

    public function showAdminBuildingAdd()
    {
        return $this->render('admin/buildings/form.twig');
    }

    public function showAdminBuildingEdit($request, $response, $args)
    {
        $building = Building::find($args['id']);
        $this->twig_vars['building'] = $building->toArray();
        return $this->render('admin/buildings/form.twig');
    }

    public function createBuilding($request, $response)
    {
        $data = $request->getParam('building');
        Building::create($data);
        return $response->withStatus(301)->withHeader('Location', $this->ci->router->pathFor('building.showAdminBuildingsList'));
    }

    public function updateBuilding($request, $response, $args)
    {
        $data = $request->getParam('building');
        $building = Building::find($args['id']);
        $building->update($data);
        return $response->withStatus(301)->withHeader('Location', $this->ci->router->pathFor('building.showAdminBuildingsList'));
    }


}