<?php


namespace App\Controllers;


use App\Models\Building;
use App\Models\Zone;
use Slim\Http\Request;
use Slim\Http\Response;

class MapController extends Controller
{
    public function showAdminMap()
    {
        $this->twig_vars['buildings'] = Building::get()->toArray();
        return $this->render('admin/map/map.twig');
    }

    public function showAdminMapEdit($request, $response, $args)
    {
        $building = Building::with('zones')->find($args['id'])->toJson();
        $this->twig_vars['building'] = $building;
        return $this->render('admin/map/edit-building.twig');
    }


    public function showAddBuilding()
    {
        return $this->render('admin/map/add-building.twig');
    }

    public function createBuilding(Request $request, Response $response)
    {
        $title =  $request->getParam('title');
        $building = Building::create(['title' => $title]);
        return $response->withJson(['id' => $building->id]);
    }

    public function loadPlan(Request $request, Response $response, $args)
    {
        if (!empty($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $file = $this->_uploadFiles('file')[0];
            Building::where('id', $request->getParam('id'))->update(
                [
                    'plan_path' => $file['path'],
                    'width' => $request->getParam('width'),
                    'height' => $request->getParam('height'),
                    'floor' => $request->getParam('floor'),
                ]
            );
            return json_encode($file);
        }
        return json_encode(false);
    }

    public function setCoords(Request $request)
    {
        $coords = $request->getParam('coords');
        Building::where('id', $request->getParam('id'))->update($coords);
    }

    public function savePolygons(Request $request)
    {
        $polygones = $request->getParam('polygons');
        $polygones = json_decode($polygones, true);
        foreach ($polygones as $polygone){
            Zone::create(
                [
                    'title' => $polygone['title'],
                    'geo' => json_encode($polygone['geo']),
                    'building_id' => $request->getParam('id')
                ]
            );
        }
    }
}