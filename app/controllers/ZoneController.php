<?php


namespace App\Controllers;


use App\Models\Zone;
use Slim\Http\Request;
use Slim\Http\Response;

class ZoneController extends Controller
{
    public function saveZone(Request $request, Response $response, $args)
    {
        $data = $request->getParams();

        $zone = Zone::create(
            [
                'title' => $data['title'],
                'geo' => $data['geo'],
                'floor_id' => $args['id']
            ]);
        return $zone->toJson();
    }
}