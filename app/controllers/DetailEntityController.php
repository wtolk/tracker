<?php

namespace App\Controllers;

use App\Models\Detail;
use App\Models\DetailEntity;
use App\Models\Storage;

class DetailEntityController extends Controller
{
//    public function showAdminDetailEntityList($request, $response, $args)
//    {
//        $details = Detail::with('storage')->get()->toArray();
//        $this->twig_vars['details'] = $details;
//        return $this->render('admin/details/details-list.twig');
//    }

    public function showAdminDetailEntityEdit($request, $response, $args)
    {
        $this->twig_vars['detail_entity'] = DetailEntity::find($args['id']);
        $this->twig_vars['details'] = Detail::all()->toArray();
        $this->twig_vars['storages'] = Storage::all()->toArray();
        return $this->render('admin/details/detail-entity-form.twig');
    }

    public function showAdminDetailEntityAdd($request, $response, $args)
    {
        $this->twig_vars['detail_id'] = $args['detail_id'];
        $this->twig_vars['storages'] = Storage::all()->toArray();
        return $this->render('admin/details/detail-entity-form.twig');
    }

    public function createDetailEntity($request, $response, $args)
    {
        $data = $request->getParams();
        DetailEntity::create($data['detail_entity']);
        return $response->withRedirect($this->ci->router->pathFor('detail.showAdminDetailList'));
    }

    public function updateDetailEntity($request, $response, $args)
    {
        $data = $request->getParams();
        DetailEntity::find($args['id'])->update($data['detail_entity']);
        return $response->withRedirect($this->ci->router->pathFor('detail.showAdminDetailList'));
    }

    public function deleteDetailEntity($request, $response, $args) {
        DetailEntity::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('detail.showAdminDetailList'));
    }

}