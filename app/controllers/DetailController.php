<?php

namespace App\Controllers;

use App\Models\Detail;
use App\Models\Storage;

class DetailController extends Controller
{
    public function showAdminDetailList($request, $response, $args)
    {
        $details = Detail::get()->toArray();
        $this->twig_vars['details'] = $details;
        return $this->render('admin/details/details-list.twig');
    }

    public function showAdminDetailEdit($request, $response, $args)
    {
        $this->twig_vars['detail'] = Detail::find($args['id']);
        $this->twig_vars['storages'] = Storage::all()->toArray();
        return $this->render('admin/details/detail-form.twig');
    }

    public function showAdminDetailAdd($request, $response, $args)
    {
        $this->twig_vars['storages'] = Storage::all()->toArray();
        return $this->render('admin/details/detail-form.twig');
    }

    public function createDetail($request, $response, $args)
    {
        $data = $request->getParams();
        Detail::create($data['detail']);
        return $response->withRedirect($this->ci->router->pathFor('detail.showAdminDetailList'));
    }

    public function updateDetail($request, $response, $args)
    {
        $data = $request->getParams();
        Detail::find($args['id'])->update($data['detail']);
        return $response->withRedirect($this->ci->router->pathFor('detail.showAdminDetailList'));
    }

    public function deleteDetail($request, $response, $args) {
        Detail::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('detail.showAdminDetailList'));
    }

}