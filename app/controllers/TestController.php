<?php

namespace App\Controllers;
use App\Helpers\FlockBot;
use App\Models\Floor;
use App\Models\RequestMovement;
use App\Models\Role;
use Illuminate\Pagination\Paginator;
use MongoDB\Client;
use Slim\Http\Request;
use Slim\Http\Response;
use App\Helpers\Dijkstra;
use App\Helpers\Graph;
use App\Helpers\Node;

class TestController extends Controller
{
    /**
     * Создает пользователя test с паролем test
     *
     * @param $request
     * @param $response
     * @param $args
     * @return mixed
     */
    public function test($request, $response, $args)
    {
        $role = Role::create(['name'=>'Тестовая','slug'=>'TestRole', 'permissions'=>'{"user_view":true,"user_create":true,"user_update":true,"user_delete":true, "roles_view":true}']);

        $credentials = [
            'email'    => 'test',
            'password' => 'test',
            'first_name' => 'Тестовый',
            'last_name' => 'Пользователь'
        ];

        $user = $this->ci['sentinel']->registerAndActivate($credentials);

        $role->users()->attach($user);

        $this->ci['flash']->addMessage('wrong', 'Тестовый пользватель создан');
        return $response->withStatus(301)->withHeader('Location', '/login');

    }

    public function rm(Request $request, Response $response, array $args)
    {
        $floor = Floor::find($args['id']);

        $client = new Client("mongodb://localhost:27017");
        $collection = $client->tracker->{$floor->uuid};
        $collection->deleteMany([]);
        ddd('ssss');
    }

    public static function getShortestPath($from_name, $to_name)
    {

        $floor = Floor::with('tops')->find(1);
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->tracker->{$floor->uuid};

        $routes = [];

        $fields = $floor->tops->pluck('id')->toArray();

        $cursor = $collection->find([], $fields);
        foreach ($cursor as $row) {
            $row = $row->getArrayCopy();
            $id = $row['id'];
            unset($row['_id']);
            unset($row['id']);
            $f = $fields;
            foreach ($row as $vertex => $price) {
                unset($f[array_search($vertex, $f)]);
                $routes[] = ['from' => $id, 'to' => $vertex, 'price' => intval($price)];
            }
        };

        $graph = new Graph();
        foreach ($routes as $route) {
            $from = $route['from'];
            $to = $route['to'];
            $price = $route['price'];
            if (!array_key_exists($from, $graph->getNodes())) {
                $from_node = new Node($from);
                $graph->add($from_node);
            } else {
                $from_node = $graph->getNode($from);
            }
            if (!array_key_exists($to, $graph->getNodes())) {
                $to_node = new Node($to);
                $graph->add($to_node);
            } else {
                $to_node = $graph->getNode($to);
            }
            $from_node->connect($to_node, $price);
        }

        $g = new Dijkstra($graph);
        $start_node = $graph->getNode($from_name);
        $end_node = $graph->getNode($to_name);
        $g->setStartingNode($start_node);
        $g->setEndingNode($end_node);
//        echo "From: " . $start_node->getId() . "\n";
//        echo "To: " . $end_node->getId() . "\n";
//        echo "Route: " . $g->getLiteralShortestPathJson() . "\n";
//        echo "Total: " . $g->getDistance() . "\n";
        return $g->getLiteralShortestPathJson();
    }
}