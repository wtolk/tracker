<?php

namespace App\Controllers;

use App\Models\Worker;
use App\Models\WorkerPosition;
use Illuminate\Database\Capsule\Manager as DB;

class WorkerController extends Controller
{
    public function showAdminWorkerList($request, $response, $args)
    {
        $workers = Worker::get()->toArray();
        $this->twig_vars['workers'] = $workers;
        return $this->render('admin/workers/workers-list.twig');
    }

    public function setWorkerPosition($request, $response, $args)
    {
        $fields = [];
        $data = $request->getParsedBody();
        $fields['worker_id'] = $data['worker_id'];
        $fields['options'] = ['latitude' => $data['latitude'], 'longitude' => $data['longitude']];
        WorkerPosition::create($fields);
        echo '200';
    }


}