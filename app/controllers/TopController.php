<?php


namespace App\Controllers;


use App\Helpers\Dijkstra;
use App\Helpers\Graph;
use App\Helpers\Node;
use App\Models\Floor;
use App\Models\Path;
use App\Models\Top;
use MongoDB\Client;
use Slim\Http\Request;
use Slim\Http\Response;

class TopController extends Controller
{
    public function saveTop(Request $request, Response $response, array $args)
    {
        $data = $request->getParams();
        $top = Top::create(
            [
                'zone_id' => $data['zone_id'] == 0 ? null : $data['zone_id'],
                'floor_id' => $args['id'],
                'geo' => $data['geo']
            ]
        );
        return $top->toJson();
    }

    public function getShortestPath($from_name, $to_name, $routes)
    {
        $graph = new Graph();
        foreach ($routes as $route) {
            $from = $route['from'];
            $to = $route['to'];
            $price = $route['price'];
            if (!array_key_exists($from, $graph->getNodes())) {
                $from_node = new Node($from);
                $graph->add($from_node);
            } else {
                $from_node = $graph->getNode($from);
            }
            if (!array_key_exists($to, $graph->getNodes())) {
                $to_node = new Node($to);
                $graph->add($to_node);
            } else {
                $to_node = $graph->getNode($to);
            }
            $from_node->connect($to_node, $price);
        }

        $g = new Dijkstra($graph);
        $start_node = $graph->getNode($from_name);
        $end_node = $graph->getNode($to_name);
        $g->setStartingNode($start_node);
        $g->setEndingNode($end_node);
        echo "From: " . $start_node->getId() . "\n";
        echo "To: " . $end_node->getId() . "\n";
        echo "Route: " . $g->getLiteralShortestPath() . "\n";
        echo "Total: " . $g->getDistance() . "\n";
    }

    public function parseFloor(Request $request, Response $response, $args)
    {
        $floor = Floor::with('tops')->find($args['id']);
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->tracker->{$floor->uuid};

        $routes = [];

        $fields = $floor->tops->pluck('id')->toArray();

        $cursor = $collection->find([], $fields);
        foreach ($cursor as $row) {
            $row = $row->getArrayCopy();
            $id = $row['id'];
            unset($row['_id']);
            unset($row['id']);
            $f = $fields;
            foreach ($row as $vertex => $price) {
                unset($f[array_search($vertex, $f)]);
//                ddd($f);
                $routes[] = ['from' => $id, 'to' => $vertex, 'price' => intval($price)];
            }
//            if (count($f)>0){
//                foreach ($f as $k){
//                    $routes[] = ['from' => $id, 'to' => $k, 'price' => PHP_INT_MAX];
//                }
//            }
        };

        ddd($routes);

        $this->getShortestPath(26, 30, $routes);

        ddd(123);
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->tracker->{'5d9006f6b46a8'};

        $vertexes = Top::where('floor_id', $args['id'])->get()->pluck('id')->toArray();
        $matrix = [];


        foreach ($vertexes as $vertex) {
            $row = $collection->findOne(['id' => strval($vertex)]);
            foreach ($vertexes as $v) {
                $matrix[$vertex][$v] = $row->{$v} ?? 0;
            }
        }

        $from = 26;
        $to = 33;


    }

    public function savePath(Request $request, Response $response, array $args)
    {
        $data = $request->getParams();
        Path::create($data);

        $floor = Floor::find($args['id']);
        $client = new Client("mongodb://localhost:27017");
        $collection = $client->tracker->{$floor->uuid};
        $first = $collection->findOne(['id' => $data['f_top_id']]);
        if (!$first) {
            $result = $collection->insertOne(['id' => $data['f_top_id'], $data['s_top_id'] => $data['score']]);
        } else {
            $collection->updateOne(
                ['id' => $data['f_top_id']],
                ['$set' => [$data['s_top_id'] => $data['score']]]
            );
        }

        $second = $collection->findOne(['id' => $data['s_top_id']]);

        if (!$second) {
            $result = $collection->insertOne(['id' => $data['s_top_id'], $data['f_top_id'] => $data['score']]);
        } else {
            $collection->updateOne(
                ['id' => $data['s_top_id']],
                ['$set' => [$data['f_top_id'] => $data['score']]]
            );
        }
    }
}