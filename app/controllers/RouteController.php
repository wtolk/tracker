<?php


namespace App\Controllers;


use App\Helpers\Router;
use App\Models\Floor;
use App\Models\Path;
use App\Models\Top;
use App\Models\Zone;
use Slim\Http\Request;
use Slim\Http\Response;

class RouteController extends Controller
{
    public function getRouteWithMap(Request $request, Response $response)
    {
        $data = $request->getParams();
        $from = Top::where('zone_id', $data['from'])->first();
        $to = Top::where('zone_id', $data['to'])->first();

        $floor = Floor::with('zones')->find($from->floor_id);

        // Получить зоны

        // Получить метки
        $tops_id = Router::dstr($from->floor_id, $from->id, $to->id)['routing'];
        $tops = Top::find($tops_id);
        $tops = $tops->sortBy(function($model) use ($tops_id) {
            return array_search($model->getKey(), $tops_id);
        });

        $paths = [];
        for($i = 0; $i < count($tops_id); $i++){
            $path = Path::where(
                [
                    ['f_top_id' , $tops_id[$i]],
                    ['s_top_id' , $tops_id[$i+1]]
                ]
            )->orWhere(
                [
                    ['s_top_id' ,$tops_id[$i]],
                    ['f_top_id' ,$tops_id[$i+1]]
                ]
            )->first();

            if ($path){
                $paths[] = $path->toArray();
            }
        }

        $tops = [$tops->first()->toArray(),$tops->last()->toArray()];


        return json_encode(
            [
                'floor' => $floor->toArray(),
                'paths' => $paths,
                'tops' => $tops
            ]
        );
    }
}