<?php


namespace App\Controllers;


use App\Models\Building;
use App\Models\Floor;
use App\Models\Path;
use MongoDB\Client;
use Slim\Http\Request;
use Slim\Http\Response;

class FloorController extends Controller
{
    public function showAdminFloorsList($request, $response, $args)
    {
        $building = Building::with('floors')->find($args['id']);
        $this->twig_vars['building'] = $building->toArray();
        return $this->render('admin/floors/list.twig');
    }

    public function showAdminFloorAdd($request, $response, $args)
    {
        $this->twig_vars['building'] = Building::find($args['id']);
        return $this->render('admin/floors/form.twig');
    }

    public function showAdminFloorEdit($request, $response, $args)
    {
        $this->twig_vars['building'] = Building::find($args['building_id']);
        $floor = Floor::with('zones', 'tops')->find($args['id']);
        $top_ids = $floor->tops->pluck('id')->toArray();
        $paths = Path::whereIn('f_top_id', $top_ids)->orWhereIn('s_top_id', $top_ids)->get();
        $this->twig_vars['geo'] = $paths->pluck('geo')->toArray();
        $this->twig_vars['floor'] = $floor;
        return $this->render('admin/floors/form.twig');
    }

    public function createFloor(Request $request, Response $response, $args)
    {
        $data = $request->getParam('floor');
        $data['building_id'] = $args['building_id'];

        if (!empty($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $file = $this->_uploadFiles('file')[0];
            $data['plan_path'] = $file['path'];
        }
        $data['uuid'] = uniqid('', false);
        $floor = Floor::create($data);
        $client = new Client("mongodb://localhost:27017");
        $client->tracker->createCollection($data['uuid']);
        return $response->withRedirect($this->ci->router->pathFor('floor.showAdminFloorsList', ['id' => $args['building_id']]));
    }

    public function updateFloor(Request $request, Response $response, $args)
    {
        $data = $request->getParam('floor');
        $floor = Floor::find($args['id']);

        if (!empty($_FILES['file']) && $_FILES['file']['size'] > 0) {
            $file = $this->_uploadFiles('file')[0];
            $data['plan_path'] = $file['path'];
        }
        $floor->update($data);

        return $response->withRedirect($this->ci->router->pathFor('floor.showAdminFloorsList', ['id' => $args['building_id']]));
    }

}