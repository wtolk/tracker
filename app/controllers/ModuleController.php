<?php
namespace App\Controllers;

use App\Models\Module;

class ModuleController extends Controller
{
    public function showAdminModuleList($request, $response, $args)
    {

        $modules = Module::get()->toArray();

        $app_modules = self::appModules();

        foreach($modules as $module)
        {
            foreach($app_modules as $app_key => $app_module)
            {
                if ($module['path'] == $app_module['path'])
                {
                    unset($app_modules[$app_key]);
                }
            }
        }

        foreach($app_modules as $app_module)
        {
            array_push($modules, $app_module);
        }

        $this->twig_vars['modules'] = $modules;
        $this->render('admin/modules/modules-list.twig');
    }

    public static function appModules()
    {
        $modules_dirs = glob(__DIR__.'/../modules/*', GLOB_ONLYDIR);
        $modules = [];

        foreach($modules_dirs as $modules_dir)
        {
            $files = glob($modules_dir.'/*.*');
            $convention = file_get_contents($modules_dir.'/convention.json');
            $convention = json_decode($convention, true);
            $modules[]= [
                'title' => $convention['ModuleName'],
                'status' => false,
                'path' => $modules_dir,
                'convention' => json_encode($convention)
            ];
        }

        return $modules;
    }

    public function switchModule($request, $response, $args)
    {
        $data = $request->getParams();
        $module = Module::find($data['id']);
        $module->status = $data['status'] == 0? 1: 0;
        $module->save();
    }

    public function addModule($request)
    {
        $data = $request->getParams();
        $convention = file_get_contents($data['path'].'/convention.json');
        $convention = json_decode($convention, true);
        $module= [
            'title' => $convention['ModuleName'],
            'status' => 0,
            'path' => $data['path'],
            'convention' => json_encode($convention)
        ];
        Module::create($module);
    }

}