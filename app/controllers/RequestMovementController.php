<?php

namespace App\Controllers;

use App\Models\Detail;
use App\Models\Place;
use App\Models\RequestMovement;
use App\Models\Storage;
use App\Models\Top;
use App\Models\Worker;
use App\Models\WorkerPosition;
use App\Models\Zone;
use Carbon\Carbon;
use Illuminate\Database\Capsule\Manager;



class RequestMovementController extends Controller
{
    public function showAdminRequestList($request, $response, $args)
    {
        $requests = RequestMovement::with('placeForm', 'placeTo')->get()->toArray();
        $this->twig_vars['requests'] = $requests;
        return $this->render('admin/requests/request-list.twig');
    }

    public function showAdminRequestEdit($request, $response, $args)
    {
        $this->twig_vars['request'] = RequestMovement::with('details')->find($args['id'])->toArray();
//        ddd($this->twig_vars);
        $this->twig_vars['details_list'] = Detail::with('itemsList')->get()->toArray();
        $this->twig_vars['places'] = Zone::all()->toArray();
        return $this->render('admin/requests/request-form.twig');
    }

    public function showAdminRequestAdd($request, $response, $args)
    {
        $this->twig_vars['request'] = RequestMovement::all()->toArray();
        $this->twig_vars['details_list'] = Detail::with('itemsList')->get()->toArray();
        $this->twig_vars['places'] = Zone::all()->toArray();
        return $this->render('admin/requests/request-form.twig');
    }

    public function createRequest($request, $response, $args)
    {
        $data = $request->getParams();
        //От производства формируется заявка.
        $req = RequestMovement::create($data['request']);
        $req->setDetailsList($data['details']);
        $req->save();
        //Проверяется где находятся нужные детали, есть ли они вообще в наличии
//        $req->checkQuantityDetails();

        //Находится первый свободный такелажник.
        $worker = Worker::where('status', 1)->first();
        if ($worker) {
            $req->worker_id = $worker->id;
//            $worker->status = 0;
//            $worker->update();
            $req->update();
        } else {
            echo 'Нет свободных сотрудников';
        }

        //Прокладывается маршрут
        $top_start = Top::where('zone_id', '=', $req->from)->first()->toArray()['id'];
        $top_end = Top::where('zone_id', '=', $req->to)->first()->toArray()['id'];
        $tops = json_decode(TestController::getShortestPath($top_start, $top_end), true);
        $points = Top::whereIn('id', $tops)->get();
        $sorted_points = $points->sortBy(function($model) use ($tops) {
            return array_search($model->getKey(), $tops);
        });
        $time = Carbon::parse($data['request']['delivery_time']);
        foreach ($sorted_points as $point) {
            $geo = json_decode($point->geo, true);
            $coordinates = $geo['geometry']['coordinates'];

            $position = WorkerPosition::create([
                'worker_id' => $req->worker_id,
                'options' => ['coordinates' => $coordinates],
                'request_id' => $req->id,
                'created_at' => $time->addMinutes(3)->toDateTimeString()
            ]);

            foreach ($req->details as $detail) {
                Manager::table('details_positions')->insert(['entity_id' => $detail->id, 'position_id' => $position->id]);
            }



        }

        return $response->withRedirect($this->ci->router->pathFor('request.showAdminRequestList'));
    }

    public function updateRequest($request, $response, $args)
    {
        $data = $request->getParams();
        $req = RequestMovement::find($args['id']);
        $req->update($data['request']);
        $req->setDetailsList($data['details']);
        return $response->withRedirect($this->ci->router->pathFor('request.showAdminRequestList'));
    }

    public function deleteRequest($request, $response, $args) {
        RequestMovement::destroy($args['id']);
        return $response->withRedirect($this->ci->router->pathFor('request.showAdminRequestList'));
    }

    public function getMobileRequestMovementsList($request, $response, $args) {
        return RequestMovement::with('placeForm', 'placeTo', 'details')->get()->toJson();
    }

}